package com.inzenjer.libmngmnt;

/**
 * Created by SUDHEESH on 3/29/2018.
 */

public class book {
    String username;
    String bno;
    String bname;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBno() {
        return bno;
    }

    public void setBno(String bno) {
        this.bno = bno;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public book(String username, String bno, String bname) {
        this.username = username;
        this.bno = bno;
        this.bname = bname;
    }
}
