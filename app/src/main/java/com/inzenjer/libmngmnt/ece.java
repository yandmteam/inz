package com.inzenjer.libmngmnt;

public class ece {
    String bno;
    String bname;
    String category;
    String authorname;
    String publication;
    String position;

    public ece(String bno, String bname, String category, String authorname, String publication, String count) {
        this.bno = bno;
        this.bname = bname;
        this.category = category;
        this.authorname = authorname;
        this.publication = publication;
        this.position = position;
        this.count = count;
    }

    public String getCount() {

        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    String count;

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBno() {
        return bno;
    }

    public void setBno(String bno) {
        this.bno = bno;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthorname() {
        return authorname;
    }

    public void setAuthorname(String authorname) {
        this.authorname = authorname;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public ece(String bno, String bname, String category, String authorname, String publication) {
        this.bno = bno;
        this.bname = bname;
        this.category = category;
        this.authorname = authorname;
        this.publication = publication;
    }

    public String getPosition() {
        return position;
    }
}
