package com.inzenjer.libmngmnt;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;
import static com.inzenjer.libmngmnt.constants.category;
public class ece_list extends AppCompatActivity {
    RecyclerView ece_view;
    private ece_adapter mAdapter;
    private List<ece>ecee = new ArrayList<>();
    String sh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ece_list);
        ece_view = (RecyclerView) findViewById(R.id.ece_id);
        mAdapter = new ece_adapter(ecee);
        getbook();
        ece_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        ece_view.setLayoutManager(mLayoutManager);
        ece_view.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        ece_view.setItemAnimator(new DefaultItemAnimator());
        ece_view.setAdapter(mAdapter);
        ece_view.addOnItemTouchListener(new RecyclerTouchListener(this, ece_view, new RecyclerTouchListener.ClickListener() {

            public void onClick(View view, int position) {
                ece ml = ecee.get(position);
                Toast.makeText(ece_list.this, ml.getBname() + " is selected!", Toast.LENGTH_SHORT).show();
                SharedPreferences sh = ece_list.this.getSharedPreferences("id", MODE_PRIVATE);
                SharedPreferences.Editor edsh = sh.edit();
                edsh.putString("bno", ml.getBno());
                Log.e("tehd", ml.getAuthorname());
                Log.e("tehd", ml.getBname());
                Log.e("tehd", ml.getCategory());
                Log.e("tehd", ml.getBname());
                edsh.putString("bname", ml.getBname());
                edsh.putString("category", ml.getCategory());
                edsh.putString("authorname", ml.getAuthorname());
                edsh.putString("publication", ml.getPublication());
                edsh.putString("position", ml.getPosition());
                edsh.commit();

                startActivity(new Intent(ece_list.this, ece_det.class));
            }


            public void onLongClick(View view, int position) {

            }
        }));
    }
    void getbook()
{
    RequestQueue queue = Volley.newRequestQueue(this);
    StringRequest stringRequest = new StringRequest(1, constants.ece_list, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.e("STR",response);
            parsingmethod(response);
            mAdapter.notifyDataSetChanged();
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
    }){
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String,String>params = new HashMap<>();
            params.put("category", category);
            return params;
        }
    };
    queue.add(stringRequest);

}


   public class NewClass extends AsyncTask<String, String, String> {
        String urlParameters = "";

        @Override
        protected String doInBackground(String... strings) {
            sh = Connectivity.excutePost(constants.ece_list,
                    "");
            Log.e("You are at", "" + sh);

            return sh;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        }

    public void parsingmethod(String resp) {
        try {
            JSONObject object0 = new JSONObject(resp);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();
            ecee.clear();
            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                String bno = data1.getString("book_no");
                String bname = data1.getString("book_name");
                String category = data1.getString("category");
                String authorname = data1.getString("author");
                String publication = data1.getString("publication");
                String position = data1.getString("position");

                ece ml = new ece(bno,bname,category,authorname,publication);
                ml.setPosition(position);
                ecee.add(ml);

            }

        } catch (JSONException e )
        {
            Log.e("fv",e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("mException", "qqqqqq" + e);
        }

    }

}