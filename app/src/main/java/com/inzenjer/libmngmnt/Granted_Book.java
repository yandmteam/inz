package com.inzenjer.libmngmnt;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Granted_Book extends Fragment {
    RecyclerView gran_view;
    private gran_adapter mAdapter;
    private List<granted> grann = new ArrayList<>();
    String sh;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_granted__book, container, false);
        gran_view = rootView.findViewById(R.id.req_id);

        mAdapter = new gran_adapter(grann);
        getbook();
        gran_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        gran_view.setLayoutManager(mLayoutManager);
        gran_view.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        gran_view.setItemAnimator(new DefaultItemAnimator());
        gran_view.setAdapter(mAdapter);
        gran_view.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), gran_view, new RecyclerTouchListener.ClickListener() {

            public void onClick(View view, int position) {
            }
            public void onLongClick(View view, int position) {
            }
        }));
        return rootView;
    }


    void getbook()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(1, constants.gra_list, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("STR",response);
                parsingmethod(response);
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("username",constants.username);
                return params;
            }
        };
        queue.add(stringRequest);

    }

    public void parsingmethod(String resp) {
        try {
            JSONObject object0 = new JSONObject(resp);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();
            grann.clear();
            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                String username = data1.getString("username");
                String bno = data1.getString("book_id");
                String bname = data1.getString("book_name");
                granted ml = new granted(username,bno,bname);
                grann.add(ml);
            }

        } catch (JSONException e )
        {
            Log.e("fv",e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("mException", "qqqqqq" + e);
        }
    }
}