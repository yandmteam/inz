package com.inzenjer.libmngmnt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Admin_granted extends AppCompatActivity {
    RecyclerView adgran_view;
    private admin_gran_adapter mAdapter;
    private List<grantedad> grantt = new ArrayList<>();
    String sh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_granted);
        adgran_view = findViewById(R.id.gran_id);

        mAdapter = new admin_gran_adapter(grantt);
        getbook();
        adgran_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        adgran_view.setLayoutManager(mLayoutManager);
        adgran_view.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        adgran_view.setItemAnimator(new DefaultItemAnimator());
        adgran_view.setAdapter(mAdapter);
        adgran_view.addOnItemTouchListener(new RecyclerTouchListener(this, adgran_view, new RecyclerTouchListener.ClickListener() {
            public void onClick(View view, int position) {

                grantedad bk = grantt.get(position);
                String bookname = bk.getBname();
                String username = bk.getUsername();
                rev(bookname,username);
            }
            public void onLongClick(View view, int position) {
            }
        }));
    }
    void getbook()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(1, constants.granted_list, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("STR",response);
                parsingmethod(response);
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void parsingmethod(String resp) {
        try {
            JSONObject object0 = new JSONObject(resp);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();
            grantt.clear();
            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                String username = data1.getString("username");
                String bno = data1.getString("role");
                String bname = data1.getString("book_name");
                grantedad ml = new grantedad(username,bno,bname);
                grantt.add(ml);
            }
        } catch (JSONException e )
        {
            Log.e("fv",e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("mException", "qqqqqq" + e);
        }
    }

    public void rev(final String bookname , final String username){

        RequestQueue queue = Volley.newRequestQueue(this);
        //TODO url
        String S_URL =constants.revok;
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee",response);

                        try {
                            JSONObject ss=new JSONObject(response);
                            String rt=ss.getString("error");
                            if (rt.equals("success"))
                            {
                                Toast.makeText(Admin_granted.this, "success", Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException sr){
                            Log.e("ee", "No data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("ErrorResponse", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("book_name", bookname);
                return params;
            }
        };
        queue.add(postRequest);

    }
}