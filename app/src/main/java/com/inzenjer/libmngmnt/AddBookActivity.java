package com.inzenjer.libmngmnt;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddBookActivity extends AppCompatActivity {

    EditText b_no,b_name,a_name,publication,position,count;
    Button add;
    Spinner category;
    String bn,bnaa,coun,ann,pu,po,st;
    String cate [] = {"ECE","CS","EEE","IT","MATHS","MTECH"};
    String seected_cat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);

        b_no=findViewById(R.id.bno);
        b_name=findViewById(R.id.bna);
        category=findViewById(R.id.cat);
        a_name=findViewById(R.id.ana);
        publication=findViewById(R.id.pub);
        position=findViewById(R.id.pos);
        count=findViewById(R.id.count);
        add= findViewById(R.id.add);
        final ArrayAdapter<String> s = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,cate);
        category.setAdapter(s);
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                seected_cat = cate[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
    });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bn = b_no.getText().toString();
                bnaa = b_name.getText().toString();
                ann = a_name.getText().toString();
                po = position.getText().toString();
                pu = publication.getText().toString();
                coun = count.getText().toString();
                    add();
            }
        });
    }

    public void add(){
        RequestQueue sr= Volley.newRequestQueue(this);
        StringRequest str=new StringRequest(1, constants.ad, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("fgf",response);
                if (response.contains("Successully"))
                {
                    Toast.makeText(AddBookActivity.this, "success", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(AddBookActivity.this, "Already Exist", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("fdgf",error.toString());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params=new HashMap<>();
                params.put("Bookno",bn);
                params.put("Bookname",bnaa);
                params.put("Category",seected_cat);
                params.put("Author",ann);
                params.put("Publication",pu);
                params.put("Position",po);
                params.put("Count",coun);
                return params;
            }
        };
        sr.add(str);
    }
}
