package com.inzenjer.libmngmnt;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class UpdateActivity extends AppCompatActivity {
    EditText b_no,b_name,a_name,publication,position,count;
    Button upd;
    EditText category;
    String no,name,cate,auth,pub,posi,cou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        b_no=(EditText)findViewById(R.id.bno);
        b_name=(EditText)findViewById(R.id.bna);
        category=(EditText)findViewById(R.id.cate);
        a_name=(EditText)findViewById(R.id.ana);
        publication=(EditText)findViewById(R.id.pub);
        position=(EditText)findViewById(R.id.pos);
        count=(EditText)findViewById(R.id.count);
        upd=(Button) findViewById(R.id.add);

        SharedPreferences sh = getSharedPreferences("id", MODE_PRIVATE);
        String bn = sh.getString("bno", "");
        String bnam = sh.getString("bname", "");
        String categor = sh.getString("category", "");
        String authornam = sh.getString("authorname", "");
        String publicatio = sh.getString("publication", "");
        String positio = sh.getString("position", "");
        String coun = sh.getString("count", "");

        b_no.setText(bn);
        b_no.setEnabled(false);
        b_name.setText(bnam);
        category.setText(categor);
        a_name.setText(authornam);
        publication.setText(publicatio);
        position.setText(positio);
        count.setText(coun);

        upd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                no=b_no.getText().toString();
                name=b_name.getText().toString();
                cate=category.getText().toString();
                auth=a_name.getText().toString();
                pub=publication.getText().toString();
                posi=position.getText().toString();
                cou=count.getText().toString();
                updateQ();
            }
        });
    }

    public void updateQ(){

        RequestQueue sr= Volley.newRequestQueue(this);
        StringRequest str=new StringRequest(1, constants.up, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("fgf",response);
                Toast.makeText(UpdateActivity.this, "success", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("fdgf",error.toString());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params=new HashMap<>();
                params.put("Bookno",no);
                params.put("Bookname",name);
                params.put("Category",cate);
                params.put("Author",auth);
                params.put("Publication",pub);
                params.put("Position",posi);
                params.put("Count",cou);
                return params;
            }
        };
        sr.add(str);
    }

}
