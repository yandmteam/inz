package com.inzenjer.libmngmnt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminActivity extends AppCompatActivity {
    String seserch;
    Button viewbook,reqest,granted;
    ImageButton srch;
    ArrayAdapter<String> adapter;
    List<String> ob = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        reqest=findViewById(R.id.req);
        granted=findViewById(R.id.gran);

        reqest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminActivity.this,Admin_req.class));
            }
        });

        granted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminActivity.this,Admin_granted.class));
            }
        });

        final AutoCompleteTextView search = findViewById(R.id.se);
        adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, ob);
        search.setThreshold(1);
        search.setAdapter(adapter);
        search.setTextColor(Color.RED);
        aut();
        srch = findViewById(R.id.ser);
        srch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seserch = search.getText().toString();
                fe();
            }
        });
        viewbook = (Button) findViewById(R.id.add);

        viewbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminActivity.this, ViewAllBookActivity.class));
            }
        });
    }

    public void fe() {
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        //TODO url
        String S_URL = constants.match;
        Toast.makeText(this, "get", Toast.LENGTH_SHORT).show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee", response);

                        try {
                            JSONObject ss = new JSONObject(response);

                            SharedPreferences sh = AdminActivity.this.getSharedPreferences("id", MODE_PRIVATE);
                            SharedPreferences.Editor ghg = sh.edit();
                            ghg.putString("bno" , ss.getString("book_no"));
                            ghg.putString("bname" , ss.getString("book_name"));
                            ghg.putString("category" , ss.getString("category"));
                            ghg.putString("authorname" , ss.getString("author"));
                            ghg.putString("publication" , ss.getString("publication"));
                            ghg.putString("position" , ss.getString("position"));
                            ghg.commit();

                            startActivity(new Intent(AdminActivity.this, ece_det.class));
                        } catch (JSONException sr) {
                            Log.e("ee", "No data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("book_name", seserch);
                return params;
            }
        };
          queue.add(postRequest);
    }

    private void aut() {
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        //TODO url
        String S_URL = constants.fet;
        Toast.makeText(this, "get", Toast.LENGTH_SHORT).show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee", response);
                        parsingmethod(response);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void parsingmethod(String response) {
        try {
            JSONObject object0 = new JSONObject(response);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();

            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                ob.add(data1.getString("book_name"));

            }

        } catch (JSONException e) {
            Log.e("fv", e.toString());
        }

    }
}
