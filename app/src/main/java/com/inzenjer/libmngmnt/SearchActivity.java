package com.inzenjer.libmngmnt;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SearchActivity extends Fragment {
    List<String> ob = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ImageButton srch;
String seserch;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_search, container, false);

        final AutoCompleteTextView search = rootView.findViewById(R.id.se);
        adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.select_dialog_item, ob);
        search.setThreshold(1);
        search.setAdapter(adapter);
        search.setTextColor(Color.RED);
        aut();
        srch = rootView.findViewById(R.id.ser);
        srch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seserch = search.getText().toString();
                fe();
            }
        });
        android.support.v7.widget.CardView nm =rootView. findViewById(R.id.card_view1);
        nm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "ece";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);
            }
        });

        android.support.v7.widget.CardView nm1 =rootView. findViewById(R.id.card_view2);
        nm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "eee";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);

            }
        });

        android.support.v7.widget.CardView nm2 =rootView. findViewById(R.id.card_view3);
        nm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "cs";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);
            }
        });

        android.support.v7.widget.CardView nm3 =rootView. findViewById(R.id.card_view4);
        nm3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "it";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);
            }
        });
        android.support.v7.widget.CardView nm4 =rootView. findViewById(R.id.card_view5);
        nm4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "maths";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);
            }
        });


        android.support.v7.widget.CardView nm5 =rootView. findViewById(R.id.card_view6);
        nm5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.category = "mtech";
                Intent i = new Intent(getActivity(), ece_list.class);
                startActivity(i);
            }
        });
        return rootView;
    }


    private void aut() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //TODO url
        String S_URL = constants.fet;
        Toast.makeText(getActivity(), "get", Toast.LENGTH_SHORT).show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee", response);
                        parsingmethod(response);
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void parsingmethod(String response) {
        try {
            JSONObject object0 = new JSONObject(response);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();

            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                ob.add(data1.getString("book_name"));

            }

        } catch (JSONException e) {
            Log.e("fv", e.toString());
        }

    }


    public void fe() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //TODO url
        String S_URL = constants.match;
        Toast.makeText(getActivity(), "get", Toast.LENGTH_SHORT).show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee", response);

                        try {
                            JSONObject ss = new JSONObject(response);

                            SharedPreferences sh = getActivity().getSharedPreferences("id", MODE_PRIVATE);
                            SharedPreferences.Editor ghg = sh.edit();
                            ghg.putString("bno" , ss.getString("book_no"));
                            ghg.putString("bname" , ss.getString("book_name"));
                            ghg.putString("category" , ss.getString("category"));
                            ghg.putString("authorname" , ss.getString("author"));
                            ghg.putString("publication" , ss.getString("publication"));
                            ghg.putString("position" , ss.getString("position"));
                            ghg.commit();

                            startActivity(new Intent(getActivity(), ece_det.class));
                        } catch (JSONException sr) {
                            Log.e("ee", "No data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("book_name", seserch);
                return params;
            }
        };
        queue.add(postRequest);
    }



}


