package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class admin_gran_adapter extends RecyclerView.Adapter<admin_gran_adapter.MyViewHolder> {
    private List<grantedad> granted_list;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, bno, bname;
        public MyViewHolder(View view) {
            super(view);
            username =  view.findViewById(R.id.username);
            bno =  view.findViewById(R.id.bno);
            bname =  view.findViewById(R.id.bname);
        }
    }
    public admin_gran_adapter(List<grantedad> granted_list) {
        this.granted_list = granted_list;
    }


    @Override
    public admin_gran_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.admin_gran_inflator, parent, false);
        return new admin_gran_adapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(admin_gran_adapter.MyViewHolder holder, int position) {
        grantedad ml = granted_list.get(position);
        holder.username.setText(ml.getUsername());
        holder.bno.setText(ml.getBno());
        holder.bname.setText(ml.getBname());
    }
    @Override
    public int getItemCount() {
        Log.e("bk", String.valueOf(granted_list.size()));
        return granted_list.size();
    }
}