package com.inzenjer.libmngmnt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AdminLog extends AppCompatActivity {
    EditText user,pass;
    Button log;
    String u,p,sh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_log);

        user=findViewById(R.id.uuser);
        pass=findViewById(R.id.ppass);
        log= findViewById(R.id.btn2);

        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                u = user.getText().toString();
                p = pass.getText().toString();
                if (u.equals("")||p.equals("")){
                    Toast.makeText(AdminLog.this, "Field Vaccant", Toast.LENGTH_SHORT).show();
                }
                else {
                    logg();
                }
            }
        });
    }

    public void logg(){

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        //TODO url
        String S_URL =constants.adminlog;
        Toast.makeText(this, "passing", Toast.LENGTH_SHORT).show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("ee",response);

                        try {
                            JSONObject ss=new JSONObject(response);
                            String rt=ss.getString("error");
                            if (rt.equals("success"))
                            {
                                Toast.makeText(AdminLog.this, "success", Toast.LENGTH_SHORT).show();

                                startActivity(new Intent(AdminLog.this,AdminActivity.class));
                            }
                        }
                        catch (JSONException sr){
                            Log.e("eee", "No data");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", u);
                params.put("password", p);
                return params;
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

}
