package com.inzenjer.libmngmnt;

/**
 * Created by SUDHEESH on 3/12/2018.
 */

public class mde_cs {
    String bname,position;

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public mde_cs(String bname, String position) {
        this.bname = bname;
        this.position = position;
    }
}
