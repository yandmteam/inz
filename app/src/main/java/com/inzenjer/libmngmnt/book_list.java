package com.inzenjer.libmngmnt;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.inzenjer.libmngmnt.constants.category;
import static com.inzenjer.libmngmnt.constants.position;

public class book_list extends Fragment {
    View rootView;
    RecyclerView book_view;
    private book_adapter mAdapter;
    private List<book> bookk = new ArrayList<>();
    String sh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_reqested, container, false);

        book_view = (RecyclerView)rootView.findViewById(R.id.book_id);

        mAdapter = new book_adapter(bookk);
        getbook();
        book_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        book_view.setLayoutManager(mLayoutManager);
        book_view.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        book_view.setItemAnimator(new DefaultItemAnimator());
        book_view.setAdapter(mAdapter);
        book_view.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), book_view, new RecyclerTouchListener.ClickListener() {

            public void onClick(View view, int position) {
                /*book ml = bookk.get(position);
                Toast.makeText(getActivity(), ml.getBname() + " is selected!", Toast.LENGTH_SHORT).show();
                SharedPreferences sh = getActivity().getSharedPreferences("id", MODE_PRIVATE);
                SharedPreferences.Editor edsh = sh.edit();
                edsh.putString("bno", ml.getBno());
                Log.e("tehd", ml.getBname());
                Log.e("tehd", ml.getBname());
                edsh.putString("username", ml.getUsername());
                edsh.putString("bno", ml.getBno());
                edsh.putString("bname", ml.getBname());
                *//*edsh.putString("position", ml.getPosition());*//*
                edsh.commit();
*/

            }


            public void onLongClick(View view, int position) {

            }
        }));
return rootView;
    }


    void getbook()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(1, constants.book_list, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("STR",response);
                parsingmethod(response);
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("username",constants.username);
                return params;
            }
        };
        queue.add(stringRequest);

    }

    public void parsingmethod(String resp) {
        try {
            JSONObject object0 = new JSONObject(resp);
            JSONObject jobject1 = object0.getJSONObject("event");
            JSONArray ja = jobject1.getJSONArray("details");
            int length = ja.length();
            bookk.clear();
            for (int i = 0; i < length; i++) {
                JSONObject data1 = ja.getJSONObject(i);
                String username = data1.getString("username");
                String bno = data1.getString("book_id");
                String bname = data1.getString("book_name");

                /*String position = data1.getString("position");*/

                book ml = new book(username,bno,bname);
                /*ml.setPosition(position);*/
                bookk.add(ml);

            }

        } catch (JSONException e )
        {
            Log.e("fv",e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("mException", "qqqqqq" + e);
        }
    }
            }