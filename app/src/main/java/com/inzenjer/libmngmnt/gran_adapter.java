package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class gran_adapter extends RecyclerView.Adapter<gran_adapter.MyViewHolder> {

    private List<granted> gran_list;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, bno, bname;

        public MyViewHolder(View view) {
            super(view);
            username = (TextView) view.findViewById(R.id.username);
            bno = (TextView) view.findViewById(R.id.bno);
            bname = (TextView) view.findViewById(R.id.bname);
        }
    }


    public gran_adapter(List<granted> gran_list) {
        this.gran_list = gran_list;
    }


    @Override
    public gran_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gran_inflator, parent, false);
        return new gran_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(gran_adapter.MyViewHolder holder, int position) {
        granted ml = gran_list.get(position);
        holder.username.setText(ml.getUsername());
        holder.bno.setText(ml.getBno());
        holder.bname.setText(ml.getBname());

    }

    @Override
    public int getItemCount() {
        Log.e("bk", String.valueOf(gran_list.size()));
        return gran_list.size();

    }
}