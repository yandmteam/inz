package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
public class admin_adapter extends RecyclerView.Adapter<admin_adapter.MyViewHolder> {

    private List<admin> admin_req;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, bno, bname;

        public MyViewHolder(View view) {
            super(view);
            username =  view.findViewById(R.id.username);
            bno =  view.findViewById(R.id.bno);
            bname =  view.findViewById(R.id.bname);
        }
    }
    public admin_adapter(List<admin> admin_req) {
        this.admin_req = admin_req;
    }
    @Override
    public admin_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.admin_inflator, parent, false);
        return new admin_adapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        admin ml = admin_req.get(position);
        holder.username.setText(ml.getUsername());
        holder.bno.setText(ml.getBno());
        holder.bname.setText(ml.getBname());
    }
    @Override
    public int getItemCount() {
        Log.e("bk", String.valueOf(admin_req.size()));
        return admin_req.size();
    }    }