package com.inzenjer.libmngmnt;
public class admin {
    String username;
    String bno;
    String bname;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getBno() {
        return bno;
    }
    public void setBno(String bno) {
        this.bno = bno;
    }
    public String getBname() {
        return bname;
    }
    public void setBname(String bname) {
        this.bname = bname;
    }
    public admin(String username, String bno, String bname) {
        this.username = username;
        this.bno = bno;
        this.bname = bname;
    }
}
