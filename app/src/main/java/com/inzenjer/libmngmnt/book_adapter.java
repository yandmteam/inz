package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class book_adapter extends RecyclerView.Adapter<book_adapter.MyViewHolder> {

    private List<book> book_list;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, bno,bname;

        public MyViewHolder(View view) {
            super(view);
            username =  view.findViewById(R.id.username);
            bno =  view.findViewById(R.id.bno);
            bname =  view.findViewById(R.id.bname);
        }
    }


    public book_adapter(List<book> book_list) {
        this.book_list = book_list;
    }


    @Override
    public book_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_inflator, parent, false);
        return new book_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(book_adapter.MyViewHolder holder, int position) {
        book ml = book_list.get(position);
        holder.username.setText(ml.getUsername());
        holder.bno.setText(ml.getBno());
        holder.bname.setText(ml.getBname());

    }

    @Override
    public int getItemCount() {
        Log.e("bk",String.valueOf(book_list.size()));
        return book_list.size();

    }
}