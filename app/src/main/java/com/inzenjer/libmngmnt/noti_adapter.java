package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class noti_adapter extends RecyclerView.Adapter<noti_adapter.MyViewHolder> {

    private List<notifi>notifi_list;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView username, bname, fine;

        public MyViewHolder(View view) {
            super(view);
            username =  view.findViewById(R.id.username);
            fine =  view.findViewById(R.id.fine);
            bname =  view.findViewById(R.id.bname);
        }
    }

    public noti_adapter(List<notifi> notifi_list) {
        this.notifi_list= notifi_list;
    }


    @Override
    public noti_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifi_inflator, parent, false);
        return new noti_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(noti_adapter.MyViewHolder holder, int position) {
        notifi ml = notifi_list.get(position);
        holder.username.setText(ml.getUsername());
        Log.e("heloo",ml.getUsername());
        holder.bname.setText(ml.getBname());
        holder.fine.setText(ml.getFine());
    }

    @Override
    public int getItemCount() {
        Log.e("bk", String.valueOf(notifi_list.size()));
        return notifi_list.size();

    }
}
