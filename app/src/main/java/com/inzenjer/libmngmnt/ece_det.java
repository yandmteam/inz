package com.inzenjer.libmngmnt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ece_det extends Activity {
    TextView bno, bname, category, authorname, publication,position;
    ImageButton img;
    Button reque;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ece_det);
        bno=(TextView)findViewById(R.id.na);
        bname=(TextView)findViewById(R.id.pas);
        category=(TextView)findViewById(R.id.phone);
        authorname=(TextView)findViewById(R.id.lat);
        publication=(TextView)findViewById(R.id.lon);
        position=(TextView)findViewById(R.id.po);
        reque=(Button)findViewById(R.id.req);


        reque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants.bookname = bname.getText().toString();
                constants.bno = bno.getText().toString();
                constants.bname = bname.getText().toString();
                goo();
            }
        });

        SharedPreferences sh = getSharedPreferences("id", MODE_PRIVATE);
        String bn = sh.getString("bno", "");
        String bnam = sh.getString("bname", "");
        String categor = sh.getString("category", "");
        String authornam = sh.getString("authorname", "");
        String publicatio = sh.getString("publication", "");
        String positio = sh.getString("position", "");

        bno.setText(bn);
        bname.setText(bnam);
        category.setText(categor);
        authorname.setText(authornam);
        publication.setText(publicatio);
        position.setText(positio);

        img=findViewById(R.id.im1);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void goo(){

        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        String S_URL = constants.go;
        StringRequest postRequest = new StringRequest(Request.Method.POST, S_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(ece_det.this, response, Toast.LENGTH_SHORT).show();


                        try {
                            JSONObject ss=new JSONObject(response);
                            String rt=ss.getString("success");
                            if (rt.equals("Successfully Registered"))
                            {
                                Toast.makeText(ece_det.this, "successfully", Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException sr){
                            Log.e("hg","error");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ErrorResponse", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", constants.username);
                params.put("bno", constants.bno);
                params.put("bname", constants.bname);
                params.put("role", constants.select_rol);
                return params;
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);

    }
}
