package com.inzenjer.libmngmnt;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
public class ece_adapter extends RecyclerView.Adapter<ece_adapter.MyViewHolder> {

    private List<ece> ece_list;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bname, authorname;

        public MyViewHolder(View view) {
            super(view);
            bname = (TextView) view.findViewById(R.id.b_name);
            authorname = (TextView) view.findViewById(R.id.b_authorname);
        }
    }


    public ece_adapter(List<ece> ece_list) {
        this.ece_list = ece_list;
    }

    @Override
    public ece_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ece_inflator, parent, false);
        return new ece_adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ece_adapter.MyViewHolder holder, int position) {
        ece ml = ece_list.get(position);
        holder.bname.setText(ml.getBname());
        holder.authorname.setText(ml.getAuthorname());

    }

    @Override
    public int getItemCount() {
        return ece_list.size();
    }
}