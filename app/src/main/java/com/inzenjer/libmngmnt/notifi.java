package com.inzenjer.libmngmnt;

/**
 * Created by SUDHEESH on 3/30/2018.
 */

public class notifi {
    String username;
    String bname;
    String fine;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public notifi(String username, String bname, String fine) {
        this.username = username;
        this.bname = bname;
        this.fine = fine;
    }
}
