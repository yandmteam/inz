package com.inzenjer.libmngmnt;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.NOTIFICATION_SERVICE;
public class Notificationn extends Fragment{

    RecyclerView noti_view;
    private noti_adapter mAdapter;
    private List<notifi> noti = new ArrayList<>();
    String sh;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_notificationn, container, false);

        noti_view = rootView.findViewById(R.id.noti_id);

        mAdapter = new noti_adapter(noti);
        getbook();
        noti_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        noti_view.setLayoutManager(mLayoutManager);
        noti_view.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        noti_view.setItemAnimator(new DefaultItemAnimator());
        noti_view.setAdapter(mAdapter);
        noti_view.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), noti_view, new RecyclerTouchListener.ClickListener() {

            public void onClick(View view, int position) {
                /*book ml = bookk.get(position);
                Toast.makeText(getActivity(), ml.getBname() + " is selected!", Toast.LENGTH_SHORT).show();
                SharedPreferences sh = getActivity().getSharedPreferences("id", MODE_PRIVATE);
                SharedPreferences.Editor edsh = sh.edit();
                edsh.putString("bno", ml.getBno());
                Log.e("tehd", ml.getBname());
                Log.e("tehd", ml.getBname());
                edsh.putString("username", ml.getUsername());
                edsh.putString("bno", ml.getBno());
                edsh.putString("bname", ml.getBname());
                *//*edsh.putString("position", ml.getPosition());*//*
                edsh.commit();
*/

            }


            public void onLongClick(View view, int position) {

            }
        }));
        return rootView;
    }

    void getbook()
    {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(1, constants.noti_list, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("STR",response);
                parsingmethod(response);
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put("username",constants.username);
                return params;
            }
        };
        queue.add(stringRequest);

    }

    public void parsingmethod(String resp) {
        try {
            JSONArray object0 = new JSONArray(resp);
            int length = object0.length();
            noti.clear();
            for (int i = 0; i < length; i++) {
                JSONObject data1 = object0.getJSONObject(i);
                String username = data1.getString("username");
                String bname = data1.getString("book_name");
                String fine = data1.getString("fine");
                float fin = Float.parseFloat(fine);
                Toast.makeText(getActivity(), fine, Toast.LENGTH_SHORT).show();
                if (fin > 0) {
                    NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                        // Configure the notification channel.
                        notificationChannel.setDescription("Channel description");
                        notificationChannel.enableLights(true);
                        notificationChannel.setLightColor(Color.RED);
                        notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                        notificationChannel.enableVibration(true);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }


                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext(), NOTIFICATION_CHANNEL_ID);

                    notificationBuilder.setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setWhen(System.currentTimeMillis())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setTicker("Hearty365")
                            //     .setPriority(Notification.PRIORITY_MAX)
                            .setContentTitle("Your fin is" + fine + "....")
                            .setContentText("Your fin is" + fine + "....")
                            .setContentInfo("Info");

                    notificationManager.notify(/*notification id*/1, notificationBuilder.build());
                    notifi ml = new notifi(username, bname, fine);
                    noti.add(ml);
                }
            }

        } catch (JSONException e )
        {
            Log.e("fv",e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("mException", "qqqqqq" + e);
        }
    }
}
